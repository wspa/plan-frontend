import React, {Component} from 'react';
import {Route, Switch, withRouter} from 'react-router-dom';

import CalendarView from './routes/CalendarView';

import {ROUTES} from './constants';

export class App extends Component {
    render() {
        return (
            <React.Fragment>
                  <Switch>
                      <Route exact path={ROUTES.MAIN} component={CalendarView}/>
                      <Route exact path={ROUTES.CALENDAR} component={CalendarView}/>
                  </Switch>
            </React.Fragment>
        );
    }
}

export default withRouter(App);
