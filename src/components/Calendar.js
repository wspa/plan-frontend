import React from 'react';
import {connect} from 'react-redux';
import moment from 'moment';
import BigCalendar from 'react-big-calendar';

import ShowEvent from '../components/ShowEvent'

import {
    setSelectedEvent,
} from '../services/eventService';

import 'moment/locale/pl';
import "react-big-calendar/lib/css/react-big-calendar.css";

BigCalendar.setLocalizer(BigCalendar.momentLocalizer(moment))

class Calendar extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            selectedView: 'day',
            selectedDate: new Date(),
        };
    }

    sampleEvents = [
        {
            title: 'Zarządzanie infrastrukturą techniczną IT',
            type: 'ćw',
            lecturer: 'mgr inż Jakub Pizoń',
            start: new Date('2018-06-01 16:05'),
            end: new Date('2018-06-01 20:20'),
            room: '220',
            group: 'K-Z',
        },
        {
            title: 'Techniki multimedialne',
            type: 'lab',
            lecturer: 'mgr inż. Łukasz Kański',
            start: new Date('2018-06-02 8:15'),
            end: new Date('2018-06-02 10:45'),
            room: '210',
            group: 'Gry',
        },
        {
            title: 'Modelowanie i symulacja systemów',
            type: 'lab',
            lecturer: 'mgr inż. Krzysztof Król',
            start: new Date('2018-06-02 8:15'),
            end: new Date('2018-06-02 10:45'),
            room: '211',
            group: 'PIESI 1',
        },
        {
            title: 'Zaawansowane programowanie w C#',
            type: 'lab',
            lecturer: 'mgr inż. Marek Kuśmirek / mgr inż. Mirosław Szuba',
            start: new Date('2018-06-02 8:15'),
            end: new Date('2018-06-02 10:45'),
            room: '213',
            group: 'PIESI 2',
        },
        {
            title: 'Modelowanie i symulacja systemów',
            type: 'wykład',
            lecturer: 'dr Aneta Wróblewska',
            start: new Date('2018-06-02 10:50'),
            end: new Date('2018-06-02 13:20'),
            room: 'A019',
            group: 'Gry, PIESI',
        },
        {
            title: 'Techniki multimedialne',
            type: 'wykład',
            lecturer: 'dr inż. Dariusz Dobrowolski',
            start: new Date('2018-06-02 13:30'),
            end: new Date('2018-06-02 15:05'),
            room: '210',
            group: 'Gry',
        },
        {
            title: 'Przetwarzanie równoległe i rozproszone',
            type: 'wykład',
            lecturer: 'mgr inż. Kamil Zieliński',
            start: new Date('2018-06-02 13:30'),
            end: new Date('2018-06-02 15:05'),
            room: '224a',
            group: 'PIESI',
        },
        {
            title: 'Przetwarzanie równoległe i rozproszone',
            type: 'lab.',
            lecturer: 'mgr inż. Kamil Zieliński',
            start: new Date('2018-06-02 15:15'),
            end: new Date('2018-06-02 16:50'),
            room: '211',
            group: 'PIESI 1',
        },
        {
            title: 'Tworzenie aplikacji internetowych',
            type: 'lab.',
            lecturer: 'dr inż. Dariusz Dobrowolski',
            start: new Date('2018-06-02 15:15'),
            end: new Date('2018-06-02 16:50'),
            room: '213',
            group: 'PIESI 2',
        },
        {
            title: 'Projekt zespołowy systemu informatycznego',
            type: 'proj.',
            lecturer: 'mgr inż. Kamil Zieliński',
            start: new Date('2018-06-02 17:00'),
            end: new Date('2018-06-02 20:20'),
            room: '208',
            group: 'połowa roku',
        },
        {
            title: 'Projekt zespołowy systemu informatycznego',
            type: 'proj.',
            lecturer: 'mgr inż. Łukasz Kański',
            start: new Date('2018-06-02 17:00'),
            end: new Date('2018-06-02 20:20'),
            room: '210',
            group: 'połowa roku',
        },
        {
            title: 'Przetwarzanie równoległe i rozproszone',
            type: 'lab.',
            lecturer: 'mgr inż. Kamil Zieliński',
            start: new Date('2018-06-02 19:35'),
            end: new Date('2018-06-02 20:20'),
            room: '211',
            group: 'PIESI 1',
        },
    ]

    onView = view => {
        this.setState({
            selectedView: view,
        });
    }

    onNavigate = date => {
        this.setState({
            selectedDate: date,
        });
    }

    showEvent = event => {
        this.props.setSelectedEvent(event);
    }

    render() {
        const {eventsList} = this.props;
        const {selectedView, selectedDate} = this.state;
        const events = eventsList || this.sampleEvents;
        const parsedEvents = events.map(event => {
            return event;
        })

        return (
            <React.Fragment>
                <ShowEvent/>
                <BigCalendar
                    events={this.sampleEvents}
                    onSelectEvent={this.showEvent}
                    min={new Date('1/1/1 08:00')}
                    max={new Date('1/1/1 21:00')}
                    date={selectedDate}
                    onNavigate={this.onNavigate}
                    view={selectedView}
                    views={['day', 'week']}
                    onView={this.onView}
                    timeslots={5}
                    step={8}
                />
            </React.Fragment>
        );
    }
}


const mapStateToProps = store => {
    return {
        selectedEvent: store.event.selectedEvent,
        eventsList: store.event.eventsList,
    };
};

const mapDispatchToProps = {
    setSelectedEvent,
};

export default connect(mapStateToProps, mapDispatchToProps)(Calendar);
