import React from 'react';
import {connect} from 'react-redux';
import moment from 'moment';

import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
} from '@material-ui/core'

import {
    setSelectedEvent,
    clearSelectedEvent,
} from '../services/eventService';

class ShowEvent extends React.Component {

    handleClose = event => {
        this.props.clearSelectedEvent();
    }


    render() {
        const {selectedEvent: event} = this.props;
        const open = event ? true : false;

        const time = moment(event.start).format('HH:mm') + ' - ' + moment(event.end).format('HH:mm');

        return open && (
            <Dialog
                open={open}
                onClose={this.handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">{event.title}</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        {time}
                    </DialogContentText>
                    <DialogContentText id="alert-dialog-description">
                        Prowadzący: {event.lecturer}
                    </DialogContentText>
                    <DialogContentText id="alert-dialog-description">
                        Sala: {event.room}
                    </DialogContentText>
                    <DialogContentText id="alert-dialog-description">
                        Rodzaj zajęć: {event.type}
                    </DialogContentText>
                    <DialogContentText id="alert-dialog-description">
                        Grupa: {event.group}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.handleClose} color="primary">
                        Zamknij
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}

const mapStateToProps = store => {
    return {
        selectedEvent: store.event.selectedEvent,
    };
};

const mapDispatchToProps = {
    setSelectedEvent,
    clearSelectedEvent,
};

export default connect(mapStateToProps, mapDispatchToProps)(ShowEvent);
