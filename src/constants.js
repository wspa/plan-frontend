export const ROUTES = {
    MAIN: '/',
    CALENDAR: '/calendar'
};

export const URL = {
    GET: {
        PLAN: {
            //http://old.wspa.pl/files/2018-05-30_inf-nst-i-ii-iii.xlsx
            INF_NST: '/files/:year-:month-:day_inf-nst-i-ii-iii.xlsx',
        },
    },
};
