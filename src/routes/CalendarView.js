import React from 'react';
import {connect} from "react-redux";

import Calendar from '../components/Calendar'
import {loadEvents} from "../services/eventService";

export class CalendarView extends React.Component {

    getData = () => {
        this.props.loadEvents();
    }

    componentDidMount = () => {
        this.getData();
    }

    render() {
        return (
            <Calendar/>
        );
    }
}

const mapDispatchToProps = {
    loadEvents,
};

export default connect(null, mapDispatchToProps)(CalendarView);
