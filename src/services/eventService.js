import axios from 'axios';
import {URL} from '../constants';
import _ from 'lodash';

import XLSX from 'xlsx';

import {
    clearCurrentEventAction,
    clearCurrentEventsListAction,
    setCurrentEventAction,
    setCurrentEventsListAction,
} from '../store/reducers/event';

export const clearSelectedEvent = () => {
    return async (dispatch, getState) => {
        dispatch(clearCurrentEventAction());
    };
};

export const clearCurrentEventsList = () => {
    return async (dispatch, getState) => {
        dispatch(clearCurrentEventsListAction());
    };
};

const getUrl = () => {
    const url = URL.GET.PLAN.INF_NST;
    return url.replace(':year', '2018').replace(':month', '05').replace(':day', '30');
};

const parseXLSX = async (dispatch, xlsxData) => {
    const workbook = await XLSX.read(xlsxData, {type: 'buffer'});
    const data = _.map(workbook.Sheets, sheet => XLSX.utils.sheet_to_json(sheet));
    dispatch(setCurrentEventsListAction(data));
};

export const loadEvents = () => {
    return async (dispatch, getState) => {
        return await axios({
            method: 'GET',
            url: getUrl(),
            responseType: 'arraybuffer'
        }).then(response => {
            const {data} = response;
            parseXLSX(dispatch, data);
            return true;
        }).catch(error => {
            ;
        });
    };
};

export const setSelectedEvent = (event) => {
    return async (dispatch, getState) => {
        dispatch(setCurrentEventAction(event));
    };
};