import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import {routerMiddleware} from 'react-router-redux';
import {createHashHistory} from 'history';

import rootReducer from './rootReducer';

const reduxRouterMiddleware = routerMiddleware(createHashHistory());
const middlewares = applyMiddleware(thunk, reduxRouterMiddleware);

const feedStore = () => {
    return {};
};

const store = createStore(
    rootReducer,
    feedStore(),
    compose(
        middlewares,
        window.devToolsExtension ? window.devToolsExtension() : f => f,
    ),
);

export default store;
