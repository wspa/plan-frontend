import {handleActions, createAction} from 'redux-actions';

import {
    SET_CURRENT_EVENT,
    CLEAR_CURRENT_EVENT,
    CLEAR_CURRENT_EVENTS_LIST,
    SET_CURRENT_EVENTS_LIST,
} from '../actionConstants';

//actions
export const setCurrentEventAction = createAction(SET_CURRENT_EVENT);
export const clearCurrentEventAction = createAction(CLEAR_CURRENT_EVENT);
export const setCurrentEventsListAction = createAction(SET_CURRENT_EVENTS_LIST);
export const clearCurrentEventsListAction = createAction(CLEAR_CURRENT_EVENTS_LIST);

//reducer
const defaultState = {
    selectedEvent: false,
    eventsList: [],
};

export default handleActions({
    [SET_CURRENT_EVENT]: (state, action) => {
        return Object.assign({}, state, {selectedEvent: action.payload});
    },
    [CLEAR_CURRENT_EVENT]: (state, action) => {
        return Object.assign({}, state, {selectedEvent: false});
    },
    [SET_CURRENT_EVENTS_LIST]: (state, action) => {
        return Object.assign({}, state, {currentEventsList: action.payload});
    },
    [CLEAR_CURRENT_EVENTS_LIST]: (state, action) => {
        return Object.assign({}, state, {currentEventsList: []});
    },
}, defaultState);
