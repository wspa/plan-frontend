import {combineReducers} from 'redux';

import event from './reducers/event';

export default combineReducers({
    event,
});
